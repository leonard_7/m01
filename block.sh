if [ $# -eq 0 ]; then
	echo "Error: num de arguments"
	echo "Usage: $0 algun argument"
	exit 1
fi
mas_de_tres=0
for arg in $*
do
 	echo "$arg" | grep -q {3,}
  	if [ $? -eq 0 ]; then 
	  ((mas_de_tres++))	  
	fi
done
echo $mas_de_tres
exit 0

