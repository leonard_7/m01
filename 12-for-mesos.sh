#!/bin/bash
#@leonard_7
#descripcio: validar meses  y mostrar sus dias
#---------------------------
#1)Validar arguments
if [ $# -eq 0 ]; then
	echo "Error num args"
	echo "Usage: $0 mesos..."
	exit 1
fi
#2) iterar la llista de arguments
for mes in $*
do
	if ! [ $mes -ge 1 -a $mes -le 12 ]; then
		echo "Error: nota $mes no valida (1-12)" >> /dev/stderr
	else
		if [ $mes -eq 2 ]; then
			echo "28"
		elif [ $mes -eq 4 -o $mes -eq 6 -o $mes -eq 9 -o $mes -eq 11 ]; then
			echo "30"
		else
			echo "31"


		fi
	fi
done
exit 0
