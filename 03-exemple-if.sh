#! /bin/bash
# @leonard_7 ASIX-M01
# Gener 2024
#
# exemples ordre if
# --------------------------------------
#1) validar arguments
if [ $# -ne 1 ]
then 
  echo "ERROR: num args incorrecte"
  echo "Usage: $0 edat"
  exit 1
fi
#2) xixa 
edat=$1
if [ $edat -ge 18 ]
then
  echo "edat $edat es major d'edat"
fi
exit 0
