#! /usr/bin
# @leonard_7 ASIX M01-ISO
# Curs 2023-2024
# Exemples case
# --------------------------
case $1 in
	[aeiou])
		echo "$1 es una vocal"
		;;
	[bcdfghjklmnpqrstvwxyz])
		echo "$1 es una consonant"
		;;
	*)
		echo "$1 es alguna otra cosa"
		;;
esac
exit 0

