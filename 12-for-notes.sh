#!/bin/bash
#@leonard_7
#descripcio:programa que rep almenys una nota o mes i per cada nota diu si es supes aprovat 
#---------------------------
#1)Validar arguments
if [ $# -eq 0 ]; then
	echo "Error num args"
	echo "Usage: $0 nota..."
	exit 1
fi
#2) iterar la llista de arguments
for nota in $*
do
	if ! [ $nota -gt 0 -a $nota -le 10 ]; then
		echo "Error: nota $nota no valida (0-10)" >> /dev/stderr 2> /dev/null
	else
		if [ $nota -lt 5 ]; then
			echo "suspes"
		elif [ $nota -lt 7 ]; then
			echo "aprovat"
		else
			echo "notable"
		fi
	fi
done
exit 0

