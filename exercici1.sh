#!/bin/bash 
#@leonard_7 Febrer 2024
#descripcio:exemples while
#--------------------------
#exercici9
while read -r line
do	
	if [ $(echo $line) == $(cut -d: -f1 /etc/passwd | grep $line) ]; then
		echo "$line"
	else
		echo "Error: nom no trobat" >> /dev/stderr 2> /dev/null
	fi
done
exit 0
#exercici 8
if [ $# -eq 0 ]; then
	echo "Error num args"
	echo "Usage: $0 noms d'usuaris"
	exit 1
fi
for nom in $*
do
	if [ $nom = $(cut -d: -f1 /etc/passwd | grep $nom) ]; then
		echo "$nom"
	else
		echo "El nom $nom no existe"
fi
done
exit 0
# exercici 7
while read -r line
do
	if [ $(echo $line | wc -L) -gt 60 ]; then
	echo $line 
	fi
done
exit 0
# exercici 6

if [ $# -eq 0 ]; then
	echo "Error num args"
	echo "Usage: $0 dias"
	exit 1
fi
laborable=0
festius=0
for dia in $*
do
case $dia in
	"dilluns" | "dimarts" | "dimecres" | "dijous" | "divendres")
		((laborable++));;
	"dissabte" | "diumenge")
		((festius++));;
	*)
	
		echo "Error: $dia no valid(dia de la semana) " >> /dev/stderr 2> /dev/null
esac
done
echo "Dies laborables: $laborable" 
echo "Dies festius: $festius"
exit 0

# exercici 5
while read -r line
do
	echo "$line" | cut -c 1-50
done
exit 0
#exercici 4

if [ $# -eq 0 ]; then
	echo "Error num args"
	echo "Usage: $0 mesos"
 	exit 1
fi

laborable=0
festius=0
for dia in $*
do
case $dia in
	"dilluns" | "dimarts" | "dimecres" | "dijous" | "divendres")
		((laborable++));;
	"dissabte" | "diumenge")
		((festius++));;
	*)
	
		echo "Error: $dia no valid(dia de la semana) " >> /dev/stderr 2> /dev/null
esac
done
echo "Dies laborables: $laborable" 
echo "Dies festius: $festius"
exit 0


# exercici 5
while read -r line
do
	echo "$line" | cut -c 51-
done
exit 0
#exercici 4

if [ $# -eq 0 ]; then
	echo "Error num args"
	echo "Usage: $0 mesos"
 	exit 1
fi

for mes in $*
do
case $mes in 
	"2")
			echo "28 dies";;
	"4" | "6" | "9" | "11")
		       echo "30 dies";;
     "1" | "3" | "5" | "7" | "8" | "10" | "12")        
	 		echo "31 dies";;
	*)
	echo "Error: $mes no valid (1-12)" >> /dev/stderr 2> /dev/null

esac
done
exit 0


#exercici 3
if [ $# -ne 1 ]; then
       echo "Error numero de arguments"
       echo "Usage $0  ..."   
       exit 1    
fi

arg=$1
num=0
while [ $num -le $arg ]
do
	echo "$num"
	((num++))
done
exit 0

# exercici 2
# iterar llista de arguments
num=1
for arg in $* 
do   
	echo "$num: $arg" 
	((num++))
done 
exit 0
# exercici 1
num=1
while read -r line
do
	echo "$num: $line"
	((num++))
done
exit 0






