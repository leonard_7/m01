#!/bin/bash
#Leonrad_7
#M01 27-02-2024
#---------------------------------------------
#Processar arguments
#1) processar els arguments i mostrar per stdout només els de 4 o més caràcters
if [ $# -eq 0 ]; then
	echo "Error: num de arguments"
	echo "Usage: $0 algun argument"
	exit 1
fi
for arg in $*
do
	echo $arg | grep {4,}
done
exit 0

#2) processar els arguments i comptar quantes n'hi ha de 3 o més caràcters.
if [ $# -eq 0 ]; then
	echo "Error: num de arguments"
		exit 1
#2) processar els arguments i comptar quantes n'hi ha de 3 o més caràcters.
if [ $# -eq 0 ]; then
	echo "Error: num de arguments"
	echo "Usage: $0 algun argument"
	exit 1
fi
mas_de_tres=0
for arg in $*
do
 	echo "$arg" | egrep ^.{3}$
	if [ $? -eq 0 ]; then
  		((mas_de_tres++))	  
	fi	
done
echo $mas_de_tres
exit 0


#
#3) processar arguments que son matricules
	#a)llistar les valides, del tipus : 9999-AAA
	#b)stdout les que son valides ,per stderr les no valides. Retorna de status el numero d'erros(novalides)
cont=0
for matricula in "$@"
do
	echo "$matricula" | egrep "^[0-9]{4}-[A-Z]{3}$"
	if [ $? -ne 0 ];
	then
		echo "ERROR : $matricula matricula no valida "
		(($cont++))
	fi
done
exit $cont

#processard stdin
#4)

#5)
#6)
ok=0
while read -r line
do
	nom=$(echo $line | cut -c1)
	cognom=$(echo $line | cut -d' ' -f2)
	echo "$nom. $cognom"
done
exit $ok
