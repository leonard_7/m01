#exercici 4

if [ $# -eq 0 ]; then
	echo "Error num args"
	echo "Usage: $0 mesos"
 	exit 1
fi

for mes in $*

case $mes in 
	"2")
			echo "28 dies";;
	"4" | "6" | "9" | "11")
		       echo "30 dies";;
     "1" | "3" | "5" | "7" | "8" | "10" | "12")        
	 		echo "31 dies";;
	*)
	echo "Error: $mes no valid (1-12)" >> /dev/stderr 2> /dev/null

esac

exit 0

