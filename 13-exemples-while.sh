#!/bin/bash
#@leonard_7 Febrer 2024
#descripcio:exemples while
#--------------------------
#8) processar linia a linia un file
fitxer=$1
num=1
while read -r line
do
	chars=$(echo $line | wc -c)
	echo "$num: ($chars) $line "| tr 'a-z' 'A-Z'
	((num++))
done < $fitxer
exit 0

#7) numerar i mostrar en majuscule
num=1
while read -r line
do
	
	echo "$num: $line" | tr 'a-z' 'A-Z'
	((num++))
done
exit 0
#6) itera linia a linia fins a token
# (per exemple FI)
TOKEN="FI"
num=1
read -r line
while [ "$line" != $TOKEN ]
do
	echo "$num: $line"
	((num++))
	read -r line
done
exit

#5)numerar les linies rebudes
contador=1
while read -r line
do
	echo "$contador: $line"
	((contador++))
done
exit 0
#4) processar stdin linia a linia
while read -r line
do
	echo $line
done
exit 0
#3) iterar la llista d'arguments (oju usar normalment for)
while [ -n "$1" ]
do
	echo "$1 $# $*"
	shift
done
exit 0
#2)comptador decrementa valor rebut
MIN=0
num=$1
while [ $num -ge $MIN ]
do
	echo -n "$num "
	((num--))
done
exit 0

#1)Mostrar numeros del 1 al 10
MAX=10
num=1
while [ $num -le $MAX ]
do
	echo -n "$num "
	((num++))
done

exit 0
