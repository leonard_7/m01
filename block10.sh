ok=0
while read -r line
do
	nom=$(echo $line | cut -c1)
	cognom=$(echo $line | cut -d' ' -f2)
	echo "$nom. $cognom"
done
exit $ok

#3) processar arguments que son matricules
	#a)llistar les valides, del tipus : 9999-AAA
	#b)stdout les que son valides ,per stderr les no valides. Retorna de status el numero d'erros(novalides)
cont=0
for matricula in "$@"
do
	echo "$matricula" | egrep "^[0-9]{4}-[A-Z]{3}$"
	if [ $? -ne 0 ]; then
		echo "ERROR : $matricula matricula no valida "
		((cont++))
	fi
done
exit $cont

