#! /bin/bash
#1
filein=$1
estatus=0
while read -r line
do
	echo $line | egrep "^[A-Z]{4}[0-9]{3}$"
	if [ $? -ne 0 ]; then
		echo "$line" >> /dev/stderr 2> /dev/null
		((errors++))
		$estatus=3
	fi

done < $filein
exit $status

