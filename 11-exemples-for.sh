#! /bin/bash
# @leonard_7 ASIX-M01 Curs 2023-2024
# Febrer 2024
# Descripcio: exemples buble for
#--------------------------------------------------------
#8)llistat las UID
llistat_login=$(cut -d: -f3 /etc/passwd | sort)
num=1
for login in $llistat_login
do
	echo "$num: $Login"
	((num++))
done
exit 0

#7) llistar numerant les lineas
llistat=$(ls)
num=1
for elem in $llistat
do
	echo "$num:$elem"
	((num++))
done
exit 0

# 6) iterar pel resultat d'executar la ordre ls
llistat=$(ls)
for elem in $llistat
do
	echo "$elem"
done
exit 0


#5) numerar arguments
num=1
for arg in "$@"
do
	echo "$num:$arg"
	((num++))
done
exit 0

#4) $@ expandeix $* no
for arg in "$@"
do
	echo "$arg"
done
exit 0

#3) iterar per la llista d'arguments
for arg in "$*"
do
	echo "$arg"
done
exit 0

#2) iterar noms
for nom in pere marta anna pau
do
	echo "$nom"
done
exit 0

#1) iterar noms
for nom in "pere" "marta" "anna" "pau"
do
	echo "$nom"
done
exit 0

