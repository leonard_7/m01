# exercici 6
if [ $# -eq 0 ]; then
	echo "Error num args"
	echo "Usage: $0 dias"
	exit 1
fi

laborable=0
festius=0
for dia in $*
do
case $dia in
	"dilluns" | "dimarts" | "dimecres" | "dijous" | "divendres")
		((laborable++));;
	"dissabte" | "diumenge")
		((festius++));;
	*)
	
        	echo "Error: $dia no valid(dia de la semana) " >> /dev/stderr 2> /dev/null
esac
done
echo "Dies laborables: $laborable" 
echo "Dies festius: $festius"
exit 0
