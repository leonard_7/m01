#! /bin/bash
# @edt ASIX M01-ISO
# prog dir...
#  a) rep un arg i és un directori i es llista
#  b) llitar numerant els elements del dir
#  c) per cada element dir si es dir, regular,
#     o altra cosa
#  d) es reben n directoris   
# ----------------------------------------------
#1) validar que hi ha un arg
ERR_NARGS=1
ERR_NODIR=2
# 1) validar arguments
if [ $# -eq 0 ]; then
  echo "Error: número args no vàlid"
  echo "usage: $0 dir..."
  exit $ERR_NARGS
fi
dir=$1

#2) Iterar per cada argument
for dir in $*
do
  if [ ! -d  $dir ];then  
    echo "ERROR: $dir Nodirectori" >> /dev/stderr
  else	 
    llista_dir=$(ls $dir)
    echo "Llistat: $dir -----------"
    for elem in $llista_dir
    do
      if [ -f  $dir/$elem ]; then
        echo -e "\t$elem is regular file"
      elif [ -d $dir/$elem ]; then
        echo -e "\t$elem is a directory"
      else
       echo -e "\t$elem is another thing"
      fi      
    done
  fi  
done
exit 0


