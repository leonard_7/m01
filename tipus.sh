#Proba_1
#
#1) erros
#2) xixa
fit=$1
if [ ! -e $fit ]; then
	echo "$fit no existeix"
elif [ -f $fit ]; then
	echo "$fit es un regular file"
elif [ -h $fit ]; then
	echo "$fit es un link"
elif [ -d $fit ]; then
	echo "$fit es un directori"
else
	echo "$fit es una altra cosa"
fi
exit 0
